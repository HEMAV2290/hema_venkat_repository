package com.gjj.igden.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.gjj.igden.model.Greeting;

@Controller
public class GreetingController {

	private static final Logger logger = LoggerFactory.getLogger(GreetingController.class);
	
  @GetMapping("/greeting")
  public String greetingForm(Model model) {
	  logger.debug("Fetching greetings");
    model.addAttribute("greeting", new Greeting());
    return "greeting";
  }

  @PostMapping("/greeting")
    public String greetingSubmit(@ModelAttribute Greeting greeting) {
	  logger.debug("Submitting greetings::" + greeting.getContent());
    return "result";
  }

}

