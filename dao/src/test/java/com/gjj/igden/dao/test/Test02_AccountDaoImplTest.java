package com.gjj.igden.dao.test;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.dao.daoimpl.AccountDaoImpl;
import com.gjj.igden.dao.daoimpl.RoleDaoImpl;
import com.gjj.igden.model.Account;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPATestConfig.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test02_AccountDaoImplTest {

    private static final Logger logger = LoggerFactory.getLogger(Test02_AccountDaoImplTest.class);
    private static final String ADMIN = "Admin"+new Date().getTime();

	@Autowired
    private AccountDaoImpl testAccountDaoImpl;
	
	@Autowired
	private RoleDaoImpl roleDaoImpl;
    
    private static Account account = null;
    private static final String ACCOUNT_NAME = "Test1"+new Date().getTime();
    
    @Before
    public void setup() {
    	try {
    		if(null != roleDaoImpl && roleDaoImpl.getCount() ==0) {
    			roleDaoImpl.createDefaultRole();
    		}
        	account = getNewAccount(ADMIN);
    		logger.debug("Test02_AccountDaoImplTest::Account before create::" + account);
			testAccountDaoImpl.create(account);
			account = testAccountDaoImpl.readByAccountName(account);
            logger.debug("Test02_AccountDaoImplTest::Account after create::" + account);
		} catch (DAOException e) {
    	    logger.error("Test02_AccountDaoImplTest::Cant create account" + e.getMessage());
			e.printStackTrace();
		}
    }
    
    @After
    public void tearDown() {
    	try {
			if(null!= account) {
			    logger.debug("Test02_AccountDaoImplTest::Account before delete::" + account);
				testAccountDaoImpl.delete(account);
			}
		} catch (DAOException e) {
    	    logger.error("Cant delete account" + e.getMessage());
			e.printStackTrace();
		}
    }

   /* @Test
    public void test01_createAccount() {
        
        try {
        	Account accountTemp = getNewAccount(ACCOUNT_NAME);
            logger.debug("Test02_AccountDaoImplTest::Account before create::" + account);
			testAccountDaoImpl.create(accountTemp);
			account = testAccountDaoImpl.readByAccountName(accountTemp);
            logger.debug("Test02_AccountDaoImplTest::Account after create::" + account);
	        Assert.assertNotNull(account);
		} catch (DAOException e) {
            logger.error("Test02_AccountDaoImplTest::Can't create account::" + e.getMessage());
			e.printStackTrace();
			Assert.assertTrue(false);
		}
        
    }*/
    
    @Test
    public void test02_readAll() {
        List<Account> accounts = testAccountDaoImpl.readAll();
        logger.debug("Test02_AccountDaoImplTest::Reading all accounts::" + accounts);
        Assert.assertTrue(accounts.size() > 0);
    }

    @Test
    public void test03_getById() throws Exception {
        Account ac = testAccountDaoImpl.read(account);
        logger.debug("Test02_AccountDaoImplTest::Account for id::" + account.getId() +" is " + ac);
        Assert.assertNotNull(ac);
    }

    @Test
    public void test04_updateAccount() throws Exception {
        Account account2 = testAccountDaoImpl.readByAccountName(account);
        String oldInfo = account2.getAdditionalInfo();
        logger.debug("Test02_AccountDaoImplTest::Account before update::" + account2);
        account2.setAdditionalInfo("test update");
        testAccountDaoImpl.update(account2);
        final String additionalInfo = testAccountDaoImpl.readByAccountName(account2).getAdditionalInfo();
        logger.debug("Test02_AccountDaoImplTest::Account after update::" + testAccountDaoImpl.readByAccountName(account2));
        Assert.assertEquals("test update", additionalInfo);
    }
    
    /*@Test
    public void test05_deleteAccount() throws Exception {
        Account account = testAccountDaoImpl.readByAccountName(ACCOUNT_NAME);
        //int originalAmount = accounts.size();
        logger.debug("Test02_AccountDaoImplTest::Account before delete::" + account);
        testAccountDaoImpl.delete(account);
        Account account2 = testAccountDaoImpl.readByAccountName(ACCOUNT_NAME);
        logger.debug("Test02_AccountDaoImplTest::Account after delete::" + account2);
		Assert.assertNull(account2);
        
    }*/
    
    private Account getNewAccount(String accountName) {
    	Account account = new Account();
        account.setAccountName(accountName);
        account.setEmail(accountName+"@test.com");
        account.setAdditionalInfo("test my "+accountName);
        account.setPassword("123qwe");
        account.addRole(roleDaoImpl.readAll().get(0));
        return account;
    }
}
